# Ben Weese

[Ben.Weese@gmail.com](mailto:Ben.Weese@gmail.com)

[BenWeese.com](https://www.benweese.com)

[Linkedin](http://www.linkedin.com/in/benweese)

# Projects

[Automated Testing](https://benweese.github.io/Automated-Testing)

# Work Experience

## MacPractice (Lincoln, NE)
*[MacPractice][] is a medical software company for Mac computers.*

**Quality Assurance Technical Lead (Team Lead)** (July 2018 - Present)

QA Technical Lead creates a technical vision for the QA department and may take on roles of the manager when needed. The Technical lead needs to keep up with how QA is testing to stay in touch. The Technical Lead focuses on the technical aspect of QA and helps with technical debates. They help to train and support the QA team as well as help them grow by finding new tools, creating trainings, and writing technical documentation. The Technical Lead also takes on task and tools as assigned such as the build system, learning and implementation of automated test for API and UI.

- **First QA doing Automated Testing**
- API Testing
- UI Testing
- Testing reports

**Quality Assurance Analyst** (August 2017 - July 2018)

Agile Testing EHR software for Macintosh computers. Writing test plans and then impending the test plans. Working in several squads or scrum style teams to help produce better designs and product by putting in tickets.

- **Automated testing Research and Demo**
- Trainings

**Quality Assurance Team Lead** (August 2014 - August 2017)

Agile Testing EHR software for Macintosh computers. Writing test plans and then impending the test plans. Working in several squads or scrum style teams to help produce better designs and product by putting in tickets. Managing employees time sheets and providing support for QA. Run static analysis on the code and fix issues pointed out by the analysis.

- Created Trainings
- Added Static Analysis
- First QA to fix bugs
- Got name in Software Devoloper box

**Quality Assurance Analyst** (September 2013 - August 2014)

Agile Testing EHR software for Macintosh computers. Writing test plans and then impending the test plans. Working in several squads or scrum style teams to help produce better designs and product by putting in tickets.

[MacPractice]: http://www.macpractice.com/
